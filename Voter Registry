import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.*;

/**
 * Registers voters after determining eligibility and maintains
 * the list of registered voters
 * @author Bonnie MacKellar
 *
 */
public class VoterRegistry {
   
   private HashMap<String, Voter> voters;
   
   public static final int ELIGAGE = 18;
   
   /**
    * create an empty voter registry
    */
   public VoterRegistry() {
	   voters = new HashMap<String, Voter>();
   }
   
 
   /**
    * register a voter, if the voter is eligible
    * date of registration will be the current date
    * @param voter
    * @return true if the voter has been registered, false otherwise
    */
   public Boolean register(Voter voter) {  
	   return register(voter, LocalDate.now());
   }
   
   /**
    * register a voter, using a registration date passed in as 
    * a parameter. This is so that voter registration data from
    * a file can be added to the registry
    * if the voter is not eligible, he or she will not be entered
    * @param voter
    * @param regDate
    * @return true if the voter has been registered, false otherwise
    */
   public Boolean register(Voter voter, LocalDate regDate) {
	   if(!valid(voter)) {
		   return false;
	   }
	   else {
		   voter.setRegDate(regDate);
		   voters.put(voter.getId(), voter);
		   
	   }
	   return true;
   }
   
   /**
    * return the number of registered voters
    */
   public int numVoters() {
	   return voters.size();
   }
   
   /**
    * remove a voter from the registry
    * @param voterId
    */
   public void remove(String voterId) {
	   voters.remove(voterId);
   }
   
   /**
    * return the voter associated with the voter id, or null if not found
    * @param voterId
    * @return a Voter or null if there is no voter with the id
    */
   public Voter find(String voterId) {
	   return voters.get(voterId);
   }

   /**
    * return a list of voters, sorted by last name
    * @return sorted list of voters
    */
   public List<Voter> getSortedList() {
	   ArrayList<Voter> sortedVoters = new ArrayList<Voter>(voters.values());
	   Collections.sort(sortedVoters, new Comparator<Voter>() {
		   public int compare(Voter v1, Voter v2){
	           return v1.getLname().compareTo(v2.getLname());
	       }
	   }
	   );
	   return sortedVoters;
		   
   }
   
private boolean valid(Voter voter) {
	// assuming voter must be 18 at registration date, and must be a citizen and not already registered
	if ((voter.getAgeAtElection(LocalDate.now()) < ELIGAGE)|| (!voter.getIsCitizen()) ||  (voters.containsKey(voter.getId()))) {
		return false;
	}
	else {
		return true; }
	
}
}